﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using Template10.Mvvm;
using Windows.UI.Xaml.Navigation;
using pamo_uwp.Services;
using pamo_uwp.Models;

namespace pamo_uwp.ViewModels
{
    class DetailsPageViewModel : ViewModelBase
    {
        public MangaDetails MangaDetails { get; set; }

        private string _MangaId = String.Empty;
        public string MangaId { get { return _MangaId; } set { Set(ref _MangaId, value); } }

        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> state)
        {
            try
            {
                if (state.Any())
                {
                    if (state.ContainsKey(nameof(MangaId))) MangaId = state[nameof(MangaId)].ToString();
                    state.Clear();
                }
                MangaDetails = await MangaDetailsService.GetMangaDetails(MangaId);
                await Task.CompletedTask;
            }
            catch (NullReferenceException e) { }
        }
    }
}
