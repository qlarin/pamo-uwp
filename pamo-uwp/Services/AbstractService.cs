﻿using System.IO;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace pamo_uwp.Services
{
    public abstract class AbstractService
    {
        protected static async Task<T> MakeRequest<T>(string url)
        {
            var http = new HttpClient();
            var response = await http.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            var serializer = new DataContractJsonSerializer(typeof(T));

            var ms = new MemoryStream(Encoding.UTF8.GetBytes(result));
            var data = (T)serializer.ReadObject(ms);
            return data;
        }
    }
}
