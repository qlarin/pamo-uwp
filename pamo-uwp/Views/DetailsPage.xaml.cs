﻿using pamo_uwp.Models;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace pamo_uwp.Views
{
    public sealed partial class DetailsPage : Page
    {
        public DetailsPage()
        {
            InitializeComponent();
            NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Disabled;
        }
    }
}
