﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace pamo_uwp.Models
{
    [DataContract]
    public class Manga
    {
        [DataMember]
        public string a { get; set; }
        [DataMember]
        public List<object> c { get; set; }
        [DataMember]
        public int h { get; set; }
        [DataMember]
        public string i { get; set; }
        [DataMember]
        public string im { get; set; }
        [DataMember]
        public int ld { get; set; }
        [DataMember]
        public int s { get; set; }
        [DataMember]
        public string t { get; set; }
    }

    [DataContract]
    public class MangaResponse
    {
        [DataMember]
        public int end { get; set; }
        [DataMember]
        public List<Manga> manga { get; set; }
        [DataMember]
        public int page { get; set; }
        [DataMember]
        public int start { get; set; }
        [DataMember]
        public int total { get; set; }
    }

    [DataContract]
    public class MangaDetails
    {
        [DataMember]
        public string alias { get; set; }
        [DataMember]
        public string author { get; set; }
        [DataMember]
        public List<string> categories { get; set; }
        [DataMember]
        public List<List<object>> chapters { get; set; }
        [DataMember]
        public int chapters_len { get; set; }
        [DataMember]
        public int created { get; set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string image { get; set; }
        [DataMember]
        public string title { get; set; }
    }
}
