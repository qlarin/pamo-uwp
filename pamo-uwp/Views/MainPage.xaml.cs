﻿using pamo_uwp.Models;
using pamo_uwp.ViewModels;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace pamo_uwp.Views
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Disabled;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button b = sender as Button;
                Manga manga = b.DataContext as Manga;
                this.Frame.Navigate(typeof(DetailsPage));
            } catch (Exception ex) { }
            
        }
    }
}