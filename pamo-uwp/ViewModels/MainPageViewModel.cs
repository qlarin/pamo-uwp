﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using Template10.Mvvm;
using Windows.UI.Xaml.Navigation;
using pamo_uwp.Services;
using pamo_uwp.Models;
using Windows.UI.Xaml.Controls;
using System.Diagnostics;

namespace pamo_uwp.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public MangaResponse MangaResponse { get; set; }
       
        private List<Manga> _MangaList = new List<Manga> { };
        public List<Manga> MangaList { get { return _MangaList; } set { Set(ref _MangaList, value); } }

        private int _Page = 0;
        public int Page { get { return _Page; } set { Set(ref _Page, value); } }
        private int _Total = 0;
        public int Total { get { return _Total; } set { Set(ref _Total, value); } }
        private int _End = 25;
        public int End { get { return _End; } set { Set(ref _End, value); } }

        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> state)
        {
            try
            {
                if (state.Any())
                {
                    if (state.ContainsKey(nameof(Page))) Page = (int)state[nameof(Page)];
                    state.Clear();
                }
                MangaResponse = await MangaListService.GetMangaList(Page);
                MangaList = MangaResponse.manga;
                Total = MangaResponse.total;
                End = MangaResponse.end;
                await Task.CompletedTask;
            }
            catch (NullReferenceException e){}
        }

        private DelegateCommand gotoPreviousPageCommand;
        public DelegateCommand GotoPreviousPageCommand => gotoPreviousPageCommand ?? (gotoPreviousPageCommand = new DelegateCommand(() =>
        {
            NavigationService.Navigate(typeof(Views.MainPage), --Page);
        }, () => Page > 0));

        private DelegateCommand gotoNextPageCommand;
        public DelegateCommand GotoNextPageCommand => gotoNextPageCommand ?? (gotoNextPageCommand = new DelegateCommand(() =>
        {
            NavigationService.Navigate(typeof(Views.MainPage), ++Page);
        }, () => End < Total));

        private DelegateCommand gotoDetailsPageCommand;
        public DelegateCommand GotoDetailsPageCommand => gotoDetailsPageCommand ?? (gotoDetailsPageCommand = new DelegateCommand(() =>
        {
            NavigationService.Navigate(typeof(Views.MainPage), ++Page);
        }, () => End < Total));
    }
}
