﻿using System;
using System.Threading.Tasks;
using pamo_uwp.Models;

namespace pamo_uwp.Services
{
    public class ChapterPagesService : AbstractService
    {
        public async static Task<ChapterDetails> GetChapterPages(string chapterId)
        {
            var url = String.Format("http://www.mangaeden.com/api/chapter/{0}/", chapterId);
            return await MakeRequest<ChapterDetails>(url);
        }
    }
}
