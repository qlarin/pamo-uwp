﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace pamo_uwp.Services
{
    public class UriConverterService : IValueConverter
    {
        public static string imageUri = "http://cdn.mangaeden.com/mangasimg/";

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            string relativepath = imageUri + (value as String);
            BitmapImage bi = new BitmapImage();
            bi.UriSource = new Uri(relativepath.Trim(), UriKind.Absolute);
            return bi;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
