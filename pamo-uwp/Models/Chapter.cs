﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace pamo_uwp.Models
{
    [DataContract]
    public class ChapterDetails
    {
        [DataMember]
        public List<List<object>> images { get; set; }
    }
}
