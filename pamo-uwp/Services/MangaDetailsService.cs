﻿using System;
using System.Threading.Tasks;
using pamo_uwp.Models;

namespace pamo_uwp.Services
{
    public class MangaDetailsService : AbstractService
    {
        public async static Task<MangaDetails> GetMangaDetails(string mangaId)
        {
            var url = String.Format("http://www.mangaeden.com/api/manga/{0}/", mangaId);
            return await MakeRequest<MangaDetails>(url);
        }
    }
}
