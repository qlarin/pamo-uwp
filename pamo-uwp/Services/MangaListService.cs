﻿using System;
using System.Threading.Tasks;
using pamo_uwp.Models;

namespace pamo_uwp.Services
{
    public class MangaListService : AbstractService
    {
        public async static Task<MangaResponse> GetMangaList(int page = 0, int limit = 25)
        {
            var url = String.Format("http://www.mangaeden.com/api/list/0/?p={0}&l={1}", page, limit);
            return await MakeRequest<MangaResponse>(url);
        }
    }
}
